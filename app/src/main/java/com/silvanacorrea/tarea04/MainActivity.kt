package com.silvanacorrea.tarea04

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {

    private  val REQUEST_GALLERY = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
            abrirGaleria()
        }

        private fun abrirGaleria(){
            fabFoto.setOnClickListener {
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    if(checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                        val permisoArchivos = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        requestPermissions(permisoArchivos,REQUEST_GALLERY)
                    }else{ //Teiene permiso
                        muestraGaleria()
                    }
                }else{ //Teiene permiso para versiones anteriores a Lollipop
                    muestraGaleria()
                }

            }
        }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
        ) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            when(requestCode){
                REQUEST_GALLERY ->{
                    if(grantResults[0]== PackageManager.PERMISSION_GRANTED)
                        muestraGaleria()
                    else
                        Toast.makeText(applicationContext, "No puedes acceder a tus imágenes",Toast.LENGTH_SHORT).show()
                }
            }
        }
        private fun muestraGaleria(){
            val intentGaleria = Intent(Intent.ACTION_PICK)
            intentGaleria.type = "image/.*"
            startActivityForResult(intentGaleria,REQUEST_GALLERY)
            //Toast.makeText(applicationContext, "Aqui van las imágenes", Toast.LENGTH_SHORT).show()
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_GALLERY){
            profile_image.setImageURI(data?.data)
        }
    }
}